FROM dockerimages/ubuntu-core
MAINTAINER https://gitlab.com/alelec/docker-android-ndk-toolchain

# Enable universe and update 
RUN sed -ri 's/^# *(.* universe)/\1/g' /etc/apt/sources.list
RUN apt update

# Install toolchain and associated requirements
RUN apt install --quiet -y build-essential bc git-core python python-pip python3 python3-pip openssh-client p7zip-full curl wget unzip && \
    pip install -U pip setuptools wheel && \
    pip3 install -U pip setuptools wheel
RUN wget --quiet https://dl.google.com/android/repository/android-ndk-r14b-linux-x86_64.zip && \
    unzip -q android-ndk-r14b-linux-x86_64.zip

# Print out installed version
RUN VERSION=$(sed -rn 's/^Pkg.Desc *= *(.*)$/\1/p' android-ndk-r14b/source.properties | sed -e 's/ /_/g')_$(sed -rn 's/^Pkg.Revision *= *(.*)$/\1/p' android-ndk-r14b/source.properties); \
   echo "VERSION=$VERSION"

ENV CROSS_COMPILE=$(pwd)/android-ndk-r14b/toolchains/aarch64-linux-android-4.9/prebuilt/linux-x86_64/bin/aarch64-linux-android-

# Clean up cache
RUN rm -rf /var/cache/apk/*