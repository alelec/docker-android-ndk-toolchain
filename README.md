----------------------------
Docker android ndk toolchain
----------------------------

Docker image for use in CI builds android kernels for 64 bit nougat devices using ndk prebuild toolchain

More info at:
* https://developer.android.com/ndk/downloads/index.html
